#!/usr/bin/env sh

if [ -s "config.h" ]; then
    doas rm -v config.h
fi

make
doas make install clean
